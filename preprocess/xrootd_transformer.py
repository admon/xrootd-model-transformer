import math
import json
import logging
import requests
from typing import *
import kfserving

from .transform_instances import Preprocessor


logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


class PreprocessTransformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.predictor_host = predictor_host
        logging.info(f"MODEL NAME {name}")
        logging.info(f"PREDICTOR URL {self.predictor_host}")
        self.preprocessor = Preprocessor()

    def preprocess(self, inputs: Dict[str, List[Dict]]) -> Dict:
        logging.info("Preprocess-Transformation called with:")
        logging.info(inputs)
        preprocessed_data = {"instances": list(map(self.preprocessor.correlate, inputs["instances"]))}
        logging.info("Preprocess-Transformation successful, calling model:predict")
        logging.info(preprocessed_data)
        return preprocessed_data

    async def predict(self, request: Dict) -> Dict:
        req_body = json.dumps(request)

        response = requests.post(
            "http://ml.cern.ch/v1/models/xrootd-model:predict",
            headers={"Host": "xrootd-model-predictor-default.admon-dev.svc.cluster.local"},
            data=req_body
        )
        logging.info(response.text)
        return response.json()

