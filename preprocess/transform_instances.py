import random
import ast
import math
from typing import List
import json
import pandas as pd
import numpy as np


class Preprocessor:
    def correlate(self, instance):
        # 1. df split between gled and enr
        # 2. sum over 15 minutes
        # 4. calculate correlation of those two df
        df = pd.DataFrame(instance["data"])
        min_time = df["time"].min()
        df["time_rounded"] = df.apply(lambda row: (row["time"] - min_time) // (60 * 15), axis=1)
        df = df.groupby(["topic", "time_rounded"]).sum()

        s_alice = df.query("topic == 'xrootd_raw_alice'")["value"]
        s_gled = df.query("topic == 'xrootd_raw_gled'")["value"]
        s_enr = df.query("topic == 'xrootd_enr_transfer'")["value"]

        # sum alice and gled to input
        s_inp = pd.merge(s_alice, s_gled, on="time_rounded").sum(axis=1)

        # calculate correlation between combined alice+gled input and enriched output
        corr = s_inp.corr(s_enr)
        return [corr if not math.isnan(corr) else 1.0]

    @staticmethod
    def df_toseries(df: pd.DataFrame) -> pd.Series:
        return df[["time_rounded", "value", "topic"]].groupby(["time_rounded", "topic"]).sum()["value"]

    @staticmethod
    def generate_example_instance():
        with open("../data/example_instances.json") as infile:
            instances = json.load(infile)

        data = []
        start_time = 1642665837
        sec = 1
        min = 60 * sec

        for which_minute in range(60):
            data.append({
                "value": random.randint(0, 100),
                "topic": "xrootd_raw_gled",
                "time": start_time + min * which_minute
            })
            data.append({
                "value": random.randint(0, 100),
                "topic": "xrootd_raw_alice",
                "time": start_time + min * which_minute
            })
            data.append({
                "value": random.randint(0, 100),
                "topic": "xrootd_enr_transfer",
                "time": start_time + min * which_minute
            })

        instances["instances"][0]["data"] = data

        with open("../data/example_instances.json", "w") as outfile:
            json.dump(instances, outfile, indent=4)


if __name__ == "__main__":
    prep = Preprocessor()

    with open("../data/example_instances.json") as infile:
        ex_inst = json.load(infile)

    prep.correlate(ex_inst["instances"][0])


