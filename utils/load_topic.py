import os
import sys
import time
import json
from dotenv import load_dotenv
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from build_schema import build_schema_deep, flatten_schema
from pyspark.sql.functions import schema_of_json, from_json, col, lit


SCALA_VERSION = "2.12"  # "2.12.10"
SPARK_VERSION = "3.2.0"  # "3.1.2"
os.environ["PYSPARK_SUBMIT_ARGS"] = f"""
    --files resources/.env,resources/kafkaclient_jaas.conf,resources/admonops.keytab,resources/keystore.jks,resources/truststore.jks 
    --driver-java-options "-Djava.security.krb5.conf=/etc/krb5.conf"
    --driver-java-options "-Djava.security.auth.login.config=resources/kafkaclient_jaas.conf" 
    --packages org.apache.spark:spark-sql-kafka-0-10_{SCALA_VERSION}:{SPARK_VERSION} main.py
"""

# Test: collectd_raw_cpu

# XRootD Model:
#  topic:xrootd_raw_alice: topic:xrootd_agg_transfer
#  topic:xrootd_raw_gled : topic:xrootd_enr_transfer

BROKER = "monit-kafkay-eed7c3fb30.cern.ch:9093,monit-kafkay-573f3ef962.cern.ch:9093,monit-kafkay-f393cbf994.cern.ch:9093,monit-kafkay-b6f6cf237c.cern.ch:9093,monit-kafkay-ce8cd4fdc8.cern.ch:9093,monit-kafkay-961c15519d.cern.ch:9093,monit-kafkay-1417749746.cern.ch:9093,monit-kafkay-d18cbd24f0.cern.ch:9093,monit-kafkay-ecfe28a060.cern.ch:9093,monit-kafkay-67acc26a80.cern.ch:9093,monit-kafkay-a29ccd22b4.cern.ch:9093,monit-kafkay-4d50c2f06a.cern.ch:9093,monit-kafkay-3f82791ad6.cern.ch:9093,monit-kafkay-d98673aa25.cern.ch:9093,monit-kafkay-b0e83e2ebc.cern.ch:9093,monit-kafkay-59d76883de.cern.ch:9093,monit-kafkay-f4c6e94a2e.cern.ch:9093,monit-kafkay-7c06bd3b6f.cern.ch:9093,monit-kafkay-1182c933d6.cern.ch:9093,monit-kafkay-e2f2dc45d2.cern.ch:9093,monit-kafkay-139aed5efb.cern.ch:9093,monit-kafkay-c0808ac86b.cern.ch:9093,monit-kafkay-ebd96dbca7.cern.ch:9093,monit-kafkay-90b46a08de.cern.ch:9093,monit-kafkay-ee4d41b68b.cern.ch:9093,monit-kafkay-7385046b84.cern.ch:9093,monit-kafkay-fe65ff1d80.cern.ch:9093,monit-kafkay-86c5d3d4ab.cern.ch:9093,monit-kafkay-844e341485.cern.ch:9093,monit-kafkay-0da67498aa.cern.ch:9093"


def load_topic(topic: str, start_offset: int, end_offset: int):
    conf = SparkConf().setAll([
        ('spark.driver.memory', '8g'),
        ('spark.memory.offHeap.enabled', 'true'),
        ('spark.memory.offHeap.size', '32g'),
    ])

    spark = SparkSession.builder.appName(
        "LoadTopic"
    ).config(conf=conf).getOrCreate()

    startOffsKey = "startingOffsetsByTimestamp"
    endOffsKey = "endingOffsetsByTimestamp"
    offsets_raw = {
        startOffsKey: {topic: {}},
        endOffsKey: {topic: {}}
    }
    for broker_idx in range(len(BROKER.split(","))):
        offsets_raw[startOffsKey][topic][broker_idx] = start_offset
        offsets_raw[endOffsKey][topic][broker_idx] = end_offset

    offsets = {
        startOffsKey: json.dumps({
            topic: offsets_raw[startOffsKey][topic]
        }),
        endOffsKey: json.dumps({
            topic: offsets_raw[endOffsKey][topic]
        })
    }

    load_dotenv("../resources/.env")
    KAFKA_CONFIG = {
        "kafka.bootstrap.servers": BROKER,
        "subscribe": topic,
        "kafka.ssl.truststore.location": "resources/truststore.jks",
        "kafka.ssl.truststore.password": os.getenv("KAFKA_PASSWORD"),
        "kafka.ssl.keystore.location": "resources/keystore.jks",
        "kafka.ssl.keystore.password": os.getenv("KAFKA_PASSWORD"),
        "kafka.ssl.key.password": os.getenv("KAFKA_PASSWORD"),
        "kafka.security.protocol": "SASL_SSL",
        "kafka.sasl.mechanism": "GSSAPI"}
    KAFKA_CONFIG.update(offsets)

    kafkaInput = spark.read.format("kafka")
    for key, val in KAFKA_CONFIG.items():
        kafkaInput = kafkaInput.option(key, val)

    df = kafkaInput.load().select(col("value").cast("string"))
    df.cache()

    sample_row = df.first()[0]
    schema = build_schema_deep(json.loads(sample_row))
    df = df.select(from_json(col("value"), schema).alias("parsed")).select("parsed.*")

    flat_select = flatten_schema(df.schema)
    df = df.select(flat_select)

    col_not_null = map(lambda c: f"{str(c)} IS NOT NULL", df.schema.fieldNames())
    fil = " OR ".join(col_not_null)
    df.filter(fil).write.mode("overwrite").parquet(f"./data/{topic}.parquet")


