import json
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.types import *


def flatten_schema(schema: StructType, prefix: str = None):
    columns = []
    for field in schema.fields:
        col_name = field.name if (prefix is None) else f"{prefix}.{field.name}"

        if isinstance(field.dataType, StructType):
            columns += flatten_schema(field.dataType, col_name)
        else:
            columns.append(col(col_name).alias(col_name))
    return columns


def build_schema_deep(json_dict: dict):
    struct_fields = []
    for column, value in json_dict.items():
        if isinstance(value, dict):
            struct_fields.append(StructField(column, build_schema_deep(value)))
        if isinstance(value, float):
            struct_fields.append(StructField(column, FloatType()))
        if isinstance(value, int):
            struct_fields.append(StructField(column, IntegerType()))
        if isinstance(value, str):
            struct_fields.append(StructField(column, StringType()))

    return StructType(struct_fields)


if __name__ == "__main__":
    spark = SparkSession.builder.getOrCreate()

    sample_json = """{"metadata":{"hostname":"monit-amqsource-7b19fbd98c.cern.ch","type_prefix":"raw","producer":"xrootd","_id":"b96a997d-d84b-0380-4e35-1e0bd9cea8f1","type":"gled","event_timestamp":1641467445000,"version":"003","timestamp":1641467446062},"data":{"unique_id":"cc84e961-6edf-11ec-ad3e-9d56b9bcbeef-9d48","file_lfn":"/replicate:4064a7ed","file_size":212696,"start_time":1641467444000,"end_time":1641467445000,"read_bytes":0,"read_operations":0,"read_min":0,"read_max":0,"read_average":0.0,"read_sigma":0.0,"read_single_bytes":0,"read_single_operations":0,"read_single_min":0,"read_single_max":0,"read_single_average":0.0,"read_single_sigma":0.0,"read_vector_bytes":0,"read_vector_operations":0,"read_vector_min":0,"read_vector_max":0,"read_vector_average":0.0,"read_vector_sigma":0.0,"read_vector_count_min":0,"read_vector_count_max":0,"read_vector_count_average":0.0,"read_vector_count_sigma":0.0,"write_bytes":0,"write_operations":0,"write_min":0,"write_max":0,"write_average":0.0,"write_sigma":0.0,"read_bytes_at_close":0,"write_bytes_at_close":212696,"user_dn":"","user_role":"","user_fqan":"","client_domain":"cern.ch","client_host":"st-096-hh151f63","server_username":"","user_protocol":"xroot","app_info":"","server_domain":"cern.ch","server_host":"st-096-hh151f63","server_site":"","vo":"atlas","activity":"w","operation":"write","ipv6":false,"remote_access":false,"is_transfer":true,"operation_time":1,"throughput":212696.0,"user":""}} """

    schema = build_schema_deep(json.loads(sample_json))
    print(schema)
    flattened_schema = flatten_schema(schema)
    print(flattened_schema)
